package br.com.restful.model;

public class SessaoVO {

    public static String usuario = "postgres";
    public static String senha = "postgres";
    public static String host = "localhost";
    public static String porta = "5432";
    public static String banco = "academico";

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        SessaoVO.usuario = usuario;
    }

    public static String getSenha() {
        return senha;
    }

    public static void setSenha(String senha) {
        SessaoVO.senha = senha;
    }

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        SessaoVO.host = host;
    }

    public static String getPorta() {
        return porta;
    }

    public static void setPorta(String porta) {
        SessaoVO.porta = porta;
    }

    public static String getBanco() {
        return banco;
    }

    public static void setBanco(String banco) {
        SessaoVO.banco = banco;
    }
}
