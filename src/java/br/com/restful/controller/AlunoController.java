package br.com.restful.controller;

import br.com.restful.dao.AlunoDAO;
import br.com.restful.model.AlunoVO;
import com.google.gson.Gson;

public class AlunoController {

    public AlunoController() throws Exception {
        new AlunoDAO().atualizarTabela();
    }

    public String consultar(String i_nome) throws Exception {
        Gson json = new Gson();

        return json.toJson(new AlunoDAO().consultar(i_nome));
    }

    public String carregar(int i_id) throws Exception {
        Gson json = new Gson();

        return json.toJson(new AlunoDAO().carregar(i_id));
    }
    
      public boolean verificadependencia(int i_id) throws Exception {
        return new AlunoDAO().verificaDependencia(i_id);
    }

    public int salvar(String j_aluno) throws Exception {
        AlunoVO oAluno = new AlunoVO();
        Gson json = new Gson();

        oAluno = json.fromJson(j_aluno, AlunoVO.class);

        new AlunoDAO().salvar(oAluno);

        return oAluno.id;
    }
    
    public String excluir(String i_id) throws Exception{
        new AlunoDAO().excluir(i_id);
        
        return "Excluído com sucesso!";
    }
}
