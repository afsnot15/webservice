package br.com.restful.controller;

import br.com.restful.dao.ConsultaCursoDAO;
import com.google.gson.Gson;

public class ConsultaCursoController {

      public String consultar() throws Exception {
        Gson json = new Gson();

        return json.toJson(new ConsultaCursoDAO().consultar());
    }
}
