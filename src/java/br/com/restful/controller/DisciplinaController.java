package br.com.restful.controller;

import br.com.restful.dao.DisciplinaDAO;
import br.com.restful.model.DisciplinaVO;
import com.google.gson.Gson;

public class DisciplinaController {

    public String consultar(String i_nome) throws Exception {
        Gson json = new Gson();

        return json.toJson(new DisciplinaDAO().consultar(i_nome));
    }

    public String carregar(int i_id) throws Exception {
        Gson json = new Gson();

        return json.toJson(new DisciplinaDAO().carregar(i_id));
    }

    public int salvar(String j_professor) throws Exception {
        DisciplinaVO oDisciplina = new DisciplinaVO();
        Gson json = new Gson();

        oDisciplina = json.fromJson(j_professor, DisciplinaVO.class);

        new DisciplinaDAO().salvar(oDisciplina);

        return oDisciplina.id;
    }

    public void excluir(String i_id) throws Exception {
        new DisciplinaDAO().excluir(i_id);
    }

    public boolean diaSemanaOcupado(int i_id, int i_diaSemana) throws Exception {
        return new DisciplinaDAO().diaSemanaOcupado(i_id, i_diaSemana);
    }

    public boolean verificadependencia(int i_id) throws Exception {
        return new DisciplinaDAO().verificaDependencia(i_id);
    }
}
