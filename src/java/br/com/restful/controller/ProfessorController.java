package br.com.restful.controller;

import br.com.restful.dao.AlunoDAO;
import br.com.restful.dao.ProfessorDAO;
import br.com.restful.model.ProfessorVO;
import com.google.gson.Gson;

public class ProfessorController {

    public ProfessorController() throws Exception {
        new ProfessorDAO().atualizarTabela();
    }

    public String consultar(String i_nome) throws Exception {
        Gson json = new Gson();

        return json.toJson(new ProfessorDAO().consultar(i_nome));
    }

    public String carregar(int i_id) throws Exception {
        Gson json = new Gson();

        return json.toJson(new ProfessorDAO().carregar(i_id));
    }

    public int salvar(String j_professor) throws Exception {
        ProfessorVO oProfessor = new ProfessorVO();
        Gson json = new Gson();

        oProfessor = json.fromJson(j_professor, ProfessorVO.class);

        new ProfessorDAO().salvar(oProfessor);

        return oProfessor.id;
    }

    public void excluir(String i_id) throws Exception {
        new ProfessorDAO().excluir(i_id);
    }

    public boolean verificadependencia(int i_id) throws Exception {
        return new ProfessorDAO().verificaDependencia(i_id);
    }
}
