package br.com.restful.controller;

import br.com.restful.dao.CursoDAO;
import br.com.restful.model.CursoVO;
import br.com.restful.model.DisciplinaVO;
import com.google.gson.Gson;
import java.util.ArrayList;

public class CursoController {

    public String consultar(String descricao) throws Exception {
        Gson json = new Gson();

        return json.toJson(new CursoDAO().consultar(descricao));
    }

    public String carregar(int i_id) throws Exception {
        Gson json = new Gson();

        return json.toJson(new CursoDAO().carregar(i_id));
    }

    public int salvar(String j_professor) throws Exception {
        CursoVO oCurso = new CursoVO();
        Gson json = new Gson();

        oCurso = json.fromJson(j_professor, CursoVO.class);

        new CursoDAO().salvar(oCurso);

        return oCurso.id;
    }

    public String excluir(String i_id) throws Exception {
        new CursoDAO().excluir(i_id);

        return "Excluído com sucesso!";
    }

    public boolean professorMestreDoutor(String j_disciplinas) throws Exception {
        ArrayList<DisciplinaVO> v_disciplinas = new ArrayList();
        Gson json = new Gson();

        DisciplinaVO[] listaDisciplina = json.fromJson(j_disciplinas, DisciplinaVO[].class);
        
         for (DisciplinaVO oDisciplina : listaDisciplina) {
            v_disciplinas.add(oDisciplina);
        }

        return new CursoDAO().professorMestreDoutor(v_disciplinas);
    }
}
