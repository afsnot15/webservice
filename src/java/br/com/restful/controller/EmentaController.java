package br.com.restful.controller;

import br.com.restful.dao.EmentaDAO;
import br.com.restful.model.EmentaVO;
import com.google.gson.Gson;

public class EmentaController {

    public EmentaController() throws Exception {
        new EmentaDAO().atualizarTabela();
    }

    public String consultar(String i_descricao) throws Exception {
        Gson json = new Gson();

        return json.toJson(new EmentaDAO().consultar(i_descricao));
    }

    public String carregar(int i_id) throws Exception {
        Gson json = new Gson();

        return json.toJson(new EmentaDAO().carregar(i_id));
    }

    public boolean verificadependencia(int i_id) throws Exception {
        return new EmentaDAO().verificaDependencia(i_id);
    }

    public int salvar(String j_ementa) throws Exception {
        EmentaVO oEmenta = new EmentaVO();
        Gson json = new Gson();

        oEmenta = json.fromJson(j_ementa, EmentaVO.class);

        new EmentaDAO().salvar(oEmenta);

        return oEmenta.id;
    }

    public String excluir(String i_id) throws Exception {
        new EmentaDAO().excluir(i_id);

        return "Excluído com sucesso!";
    }
}
