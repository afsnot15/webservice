package br.com.restful.resource;

import br.com.restful.controller.DisciplinaController;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("disciplina")
public class DisciplinaResource {

    @Context
    private UriInfo context;

    public DisciplinaResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/consultar/")
    public String consultar(@QueryParam("descricao") String i_filtro) throws Exception {
        return new DisciplinaController().consultar(i_filtro);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/carregar/{id}")
    public String carregar(@PathParam("id") int i_id) throws Exception {
        return new DisciplinaController().carregar(i_id);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/diasemanaocupado/{id}/{id_diasemana}")
    public boolean diaSemanaOcupado(@PathParam("id") int i_id, @PathParam("id_diasemana") int i_diaSemana) throws Exception {
        return new DisciplinaController().diaSemanaOcupado(i_id, i_diaSemana);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/dependencia/{id}")
    public boolean dependencia(@PathParam("id") int i_id) throws Exception {
        return new DisciplinaController().verificadependencia(i_id);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public int salvar(String j_aluno) throws Exception {
        return new DisciplinaController().salvar(j_aluno);
    }

    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("/excluir/")
    public void excluir(String i_id) throws Exception {
        new DisciplinaController().excluir(i_id);
    }
}
