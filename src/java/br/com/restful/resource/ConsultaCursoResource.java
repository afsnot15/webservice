package br.com.restful.resource;

import br.com.restful.controller.ConsultaCursoController;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("consultacurso")
public class ConsultaCursoResource {

    @Context
    private UriInfo context;

    public ConsultaCursoResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/cursodisciplina/")
    public String consultar() throws Exception {
        return new ConsultaCursoController().consultar();
    }

}
