package br.com.restful.resource;

import br.com.restful.controller.ProfessorController;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("professor")
public class ProfessorResource {

    @Context
    private UriInfo context;

    public ProfessorResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/consultar/")
    public String consultar(@QueryParam("nome") String i_filtro) throws Exception {
        return new ProfessorController().consultar(i_filtro);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/carregar/{id}")
    public String carregar(@PathParam("id") int i_id) throws Exception {
        return new ProfessorController().carregar(i_id);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public int salvar(String j_aluno) throws Exception {
        return new ProfessorController().salvar(j_aluno);
    }

    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("/excluir/")
    public void excluir(String i_id) throws Exception {
        new ProfessorController().excluir(i_id);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/dependencia/{id}")
    public boolean dependencia(@PathParam("id") int i_id) throws Exception {
        return new ProfessorController().verificadependencia(i_id);
    }
}
