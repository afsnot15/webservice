package br.com.restful.resource;

import br.com.restful.controller.CursoController;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("curso")
public class CursoResource {

    @Context
    private UriInfo context;

    public CursoResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/consultar/")
    public String consultar(@QueryParam("descricao") String i_filtro) throws Exception {
        return new CursoController().consultar(i_filtro);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/carregar/{id}")
    public String carregar(@PathParam("id") int i_id) throws Exception {
        return new CursoController().carregar(i_id);
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/professormestredoutor/{disciplinas}/")
    public boolean professorMestreDoutor(@PathParam("disciplinas")String j_disciplina) throws Exception {
        return new CursoController().professorMestreDoutor(j_disciplina);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public int salvar(String j_aluno) throws Exception {
        return new CursoController().salvar(j_aluno);
    }

    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Path("/excluir/")
    public String excluir(String i_id) throws Exception {
        return new CursoController().excluir(i_id);
    }
}
