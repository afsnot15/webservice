package br.com.restful.util;

import br.com.restful.model.SessaoVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Database {

    public static boolean bancoExiste() throws Exception {
        Conexao.conectarAuxiliar();
        Statement stm = null;
        ResultSet rst = null;
        boolean bancoExiste = false;

        stm = Conexao.getConexaoAuxiliar().createStatement();

        rst = stm.executeQuery("SELECT * FROM pg_database where datname = '" + SessaoVO.getBanco() + "';");

        if (rst.next()) {
            bancoExiste = true;
        } else {
            bancoExiste = false;
        }

        return bancoExiste;
    }

    public static void criarBanco() throws Exception {
        Conexao.conectarAuxiliar();
        Statement stm = null;

        stm = Conexao.getConexaoAuxiliar().createStatement();

        stm.execute("CREATE DATABASE " + SessaoVO.getBanco());

    }

    public static boolean tabelaExiste(String tabela) throws Exception {
        return tabelaExiste(tabela, "public");
    }

    public static boolean tabelaExiste(String i_tabela, String i_schema) throws Exception {
        Conexao.getConexao();
        Statement stm = null;
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();

        stm = Conexao.createStatement();

        sql.append("SELECT * FROM information_schema.tables");
        sql.append(" WHERE table_schema = '" + i_schema + "'");
        sql.append(" AND table_name = '" + i_tabela + "'");

        rst = stm.executeQuery(sql.toString());

        return rst.next();

    }

    public static void execute(String i_sql) throws Exception {
        PreparedStatement stm = Conexao.getConexao().prepareStatement(i_sql);

        stm.execute();
    }
    
    public static ResultSet executeQuery(String i_sql) throws Exception {
        Statement stm = Conexao.getConexao().createStatement();

        return stm.executeQuery(i_sql);
    }
}
