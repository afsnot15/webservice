package br.com.restful.util;

import br.com.restful.model.SessaoVO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Conexao {

    private static Connection conexao = null;
    private static Connection conexaoAuxiliar = null;

    public static void conectar() throws Exception {
        int tentativaConexao = 0;

        try {
            Class.forName("org.postgresql.Driver");

            conexao = DriverManager.getConnection("jdbc:postgresql://"
                    + SessaoVO.getHost() + ":"
                    + SessaoVO.getPorta() + "/"
                    + SessaoVO.getBanco(),
                    SessaoVO.getUsuario(),
                    SessaoVO.getSenha());

            conexao.setAutoCommit(false);

        } catch (Exception ex) {
            try {
                conectarAuxiliar();

                if (!Database.bancoExiste()) {
                    Database.criarBanco();
                }

                Conexao.fecharConexaoAuxiliar();

                if (tentativaConexao < 3) {
                    conectar();
                    tentativaConexao++;
                }

            } catch (Exception e) {
                throw e;
            }
        }
    }

    public static void conectarAuxiliar() throws Exception {
        Class.forName("org.postgresql.Driver");

        conexaoAuxiliar = DriverManager.getConnection("jdbc:postgresql://"
                + SessaoVO.getHost() + ":"
                + SessaoVO.getPorta()
                + "/postgres",
                SessaoVO.getUsuario(), SessaoVO.getSenha());

        conexaoAuxiliar.setAutoCommit(true);
    }

    public static void fecharConexao() throws Exception {
        conexao.close();
    }

    public static void fecharConexaoAuxiliar() throws Exception {
        conexaoAuxiliar.close();
    }

    public static Statement createStatement() throws Exception {
        return conexao.createStatement();
    }

    public static void rollback() throws Exception {
        createStatement().execute("rollback;");
    }

    public static void begin() throws Exception {
        createStatement().execute("begin;");
    }

    public static void commit() throws Exception {
        createStatement().execute("commit;");
    }

    public static Connection getConexao() throws Exception {
        return conexao;
    }

    public static Connection getConexaoAuxiliar() {
        return conexaoAuxiliar;
    }
}
