package br.com.restful.dao;

import br.com.restful.util.Conexao;
import br.com.restful.model.EmentaVO;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.util.ArrayList;

public class EmentaDAO {

    public EmentaDAO() throws Exception {
        Conexao.conectar();
    }

    public ArrayList<EmentaVO> consultar(String i_descricao) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<EmentaVO> vEmenta = new ArrayList();

        sql.append("SELECT id, descricao");
        sql.append(" FROM ementa");
        sql.append(" WHERE 1 =1");

        if (!i_descricao.isEmpty()) {
            sql.append(" AND descricao ILIKE '%" + i_descricao + "%'");
        }

        rst = Database.executeQuery(sql.toString());

        while (rst.next()) {
            EmentaVO oEmenta = new EmentaVO();

            oEmenta.id = rst.getInt("id");
            oEmenta.descricao = rst.getString("descricao");

            vEmenta.add(oEmenta);
        }

        rst.close();

        Conexao.fecharConexao();

        return vEmenta;
    }

    public EmentaVO carregar(int i_id) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        EmentaVO oEmenta = null;

        sql.append("SELECT id, descricao");
        sql.append(" FROM ementa");
        sql.append(" WHERE id = " + i_id);

        rst = Database.executeQuery(sql.toString());

        if (rst.next()) {
            oEmenta = new EmentaVO();
            oEmenta.id = rst.getInt("id");
            oEmenta.descricao = rst.getString("descricao");
        }

        rst.close();

        Conexao.fecharConexao();

        return oEmenta;
    }

    public void salvar(EmentaVO i_ementa) throws Exception {
        try {
            ResultSet rst = null;
            StringBuilder sql = new StringBuilder();

            Conexao.begin();

            if (i_ementa.id > 0) {
                sql.append("UPDATE ementa SET");
                sql.append(" descricao = '" + i_ementa.descricao + "'");
                sql.append(" WHERE id = " + i_ementa.id);
                Database.execute(sql.toString());

            } else {
                sql.append("INSERT INTO ementa (descricao) VALUES (");
                sql.append("'" + i_ementa.descricao + "' ");
                sql.append(")");

                Database.execute(sql.toString());

                rst = Database.executeQuery("SELECT CURRVAL('ementa_id_seq') AS id");
                rst.next();

                i_ementa.id = rst.getInt("id");
            }

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public void excluir(String i_id) throws Exception {
        try {
            Conexao.begin();

            Database.execute("DELETE FROM ementa WHERE id = " + i_id);

            Conexao.commit();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public boolean verificaDependencia(int i_id) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT id");
        sql.append(" FROM disciplinaementa");
        sql.append(" WHERE id_ementa = " + i_id);

        rst = Database.executeQuery(sql.toString());

        return rst.next();
    }

    public void atualizarTabela() throws Exception {
        StringBuilder sql = null;

        try {
            Conexao.begin();

            if (!Database.tabelaExiste("ementa")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE ementa (");
                sql.append(" id SERIAL, ");
                sql.append(" descricao TEXT, ");
                sql.append(" CONSTRAINT pk_id_ementa PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;
        }
    }
}
