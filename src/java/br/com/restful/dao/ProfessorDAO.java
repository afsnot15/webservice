package br.com.restful.dao;

import br.com.restful.util.Conexao;
import br.com.restful.model.ProfessorVO;
import br.com.restful.tipo.TituloProfessor;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ProfessorDAO {

    public ProfessorDAO() throws Exception {
        Conexao.conectar();
    }

    public ArrayList<ProfessorVO> consultar(String i_nome) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<ProfessorVO> vProfessor = new ArrayList();

        sql.append("SELECT p.id, p.nome, p.rg, p.cpf, p.id_titulo,");
        sql.append(" tp.descricao AS titulo");
        sql.append(" FROM professor AS p");
        sql.append(" LEFT JOIN tituloprofessor AS tp ON tp.id = p.id_titulo");
        sql.append(" WHERE 1 = 1");

        if (i_nome != null && !i_nome.isEmpty()) {
            sql.append(" AND p.nome ILIKE '%" + i_nome + "%'");
        }

        rst = Database.executeQuery(sql.toString());

        while (rst.next()) {
            ProfessorVO oProfessor = new ProfessorVO();

            oProfessor.id = rst.getInt("id");
            oProfessor.nome = rst.getString("nome");
            oProfessor.rg = rst.getString("rg");
            oProfessor.cpf = rst.getString("cpf");
            oProfessor.idTitulo = rst.getInt("id_titulo");
            oProfessor.titulo = rst.getString("titulo");

            vProfessor.add(oProfessor);
        }

        Conexao.fecharConexao();

        return vProfessor;
    }

    public ProfessorVO carregar(int i_id) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ProfessorVO oProfessor = new ProfessorVO();

        sql.append("SELECT id, nome, rg, cpf, id_titulo");
        sql.append(" FROM professor");
        sql.append(" WHERE id = " + i_id);

        rst = Database.executeQuery(sql.toString());

        if (rst.next()) {
            oProfessor.id = rst.getInt("id");
            oProfessor.nome = rst.getString("nome");
            oProfessor.rg = rst.getString("rg");
            oProfessor.cpf = rst.getString("cpf");
            oProfessor.idTitulo = rst.getInt("id_titulo");
        }

        rst.close();

        Conexao.fecharConexao();

        return oProfessor;
    }

    public void salvar(ProfessorVO i_professor) throws Exception {
        try {
            ResultSet rst = null;
            StringBuilder sql = new StringBuilder();

            Conexao.begin();

            if (i_professor.id > 0) {
                sql.append("UPDATE professor SET");
                sql.append(" nome = '" + i_professor.nome + "'");
                sql.append(", rg = '" + i_professor.rg + "'");
                sql.append(", cpf = '" + i_professor.cpf + "'");
                sql.append(", id_titulo = " + i_professor.idTitulo);
                sql.append(" WHERE id = " + i_professor.id);

                Database.execute(sql.toString());

            } else {
                sql.append("INSERT INTO professor (nome, rg, cpf, id_titulo) VALUES (");
                sql.append("'" + i_professor.nome + "', ");
                sql.append("'" + i_professor.rg + "', ");
                sql.append("'" + i_professor.cpf + "', ");
                sql.append(i_professor.idTitulo);
                sql.append(")");

                Database.execute(sql.toString());

                rst = Database.executeQuery("SELECT CURRVAL('professor_id_seq') AS id");
                rst.next();

                i_professor.id = rst.getInt("id");
            }

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public void excluir(String i_id) throws Exception {
        try {
            Conexao.begin();

            Database.execute("DELETE FROM professor WHERE id = " + i_id);

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }
    
     public boolean verificaDependencia(int i_id) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT id");
        sql.append(" FROM disciplina");
        sql.append(" WHERE id_professor = " + i_id);

        rst = Database.executeQuery(sql.toString());

        return rst.next();
    }

    public void atualizarTabela() throws Exception {
        StringBuilder sql = null;

        try {
            Conexao.begin();

            if (!Database.tabelaExiste("tituloprofessor")) {

                sql = new StringBuilder();

                sql.append(" CREATE TABLE tituloprofessor (");
                sql.append(" id INTEGER, ");
                sql.append(" descricao TEXT, ");
                sql.append(" CONSTRAINT pk_id_tituloprofessor PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());

                sql = new StringBuilder();
                sql.append("INSERT INTO tituloprofessor VALUES (");
                sql.append(TituloProfessor.ASSISTENTE.getId() + ", '");
                sql.append(TituloProfessor.ASSISTENTE.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO tituloprofessor VALUES (");
                sql.append(TituloProfessor.COLABORADOR.getId() + ", '");
                sql.append(TituloProfessor.COLABORADOR.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO tituloprofessor VALUES (");
                sql.append(TituloProfessor.MESTRE.getId() + ", '");
                sql.append(TituloProfessor.MESTRE.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO tituloprofessor VALUES (");
                sql.append(TituloProfessor.DOUTOR.getId() + ", '");
                sql.append(TituloProfessor.DOUTOR.getDescricao() + "'");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("professor")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE professor (");
                sql.append(" id SERIAL, ");
                sql.append(" nome TEXT, ");
                sql.append(" rg VARCHAR (15), ");
                sql.append(" cpf VARCHAR (15), ");
                sql.append(" id_titulo INTEGER, ");
                sql.append(" CONSTRAINT pk_id_professor PRIMARY KEY (id),");
                sql.append(" CONSTRAINT fk_id_tituloprofessor FOREIGN KEY (id_titulo) REFERENCES tituloprofessor (id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;
        }
    }
}
