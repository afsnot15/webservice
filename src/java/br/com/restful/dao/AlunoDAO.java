package br.com.restful.dao;

import br.com.restful.util.Conexao;
import br.com.restful.model.AlunoVO;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.util.ArrayList;

public class AlunoDAO {

    public AlunoDAO() throws Exception {
        Conexao.conectar();
    }

    public ArrayList<AlunoVO> consultar(String i_nome) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<AlunoVO> vAluno = new ArrayList();

        sql.append("SELECT id, nome, matricula, rg, cpf");
        sql.append(" FROM aluno");
        sql.append(" WHERE 1 =1");

        if (!i_nome.isEmpty()) {
            sql.append(" AND nome ILIKE '%" + i_nome + "%'");
        }

        sql.append(" ORDER BY id");

        rst = Database.executeQuery(sql.toString());

        while (rst.next()) {
            AlunoVO oAluno = new AlunoVO();

            oAluno.id = rst.getInt("id");
            oAluno.nome = rst.getString("nome");
            oAluno.matricula = rst.getString("matricula");
            oAluno.rg = rst.getString("rg");
            oAluno.cpf = rst.getString("cpf");

            vAluno.add(oAluno);
        }

        rst.close();

        Conexao.fecharConexao();

        return vAluno;
    }

    public AlunoVO carregar(int i_id) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        AlunoVO oAluno = null;

        sql.append("SELECT id, nome, matricula, rg, cpf");
        sql.append(" FROM aluno");
        sql.append(" WHERE id = " + i_id);

        rst = Database.executeQuery(sql.toString());

        if (rst.next()) {
            oAluno = new AlunoVO();
            oAluno.id = rst.getInt("id");
            oAluno.nome = rst.getString("nome");
            oAluno.matricula = rst.getString("matricula");
            oAluno.rg = rst.getString("rg");
            oAluno.cpf = rst.getString("cpf");
        }

        rst.close();

        Conexao.fecharConexao();

        return oAluno;
    }

    public boolean verificaDependencia(int i_id) throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT id");
        sql.append(" FROM disciplinaaluno");
        sql.append(" WHERE id_aluno = " + i_id);

        rst = Database.executeQuery(sql.toString());

        return rst.next();
    }

    public void salvar(AlunoVO i_aluno) throws Exception {
        try {
            ResultSet rst = null;
            StringBuilder sql = new StringBuilder();

            Conexao.begin();

            if (i_aluno.id > 0) {
                sql.append("UPDATE aluno SET");
                sql.append(" nome = '" + i_aluno.nome + "'");
                sql.append(", matricula = '" + i_aluno.matricula + "'");
                sql.append(", rg = '" + i_aluno.rg + "'");
                sql.append(", cpf = '" + i_aluno.cpf + "'");
                sql.append(" WHERE id = " + i_aluno.id);
                Database.execute(sql.toString());

            } else {
                sql.append("INSERT INTO aluno (nome, matricula, rg, cpf) VALUES (");
                sql.append("'" + i_aluno.nome + "', ");
                sql.append("'" + i_aluno.matricula + "', ");
                sql.append("'" + i_aluno.rg + "', ");
                sql.append("'" + i_aluno.cpf + "'");
                sql.append(")");

                Database.execute(sql.toString());

                rst = Database.executeQuery("SELECT CURRVAL('aluno_id_seq') AS id");
                rst.next();

                i_aluno.id = rst.getInt("id");
            }

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public void excluir(String i_id) throws Exception {
        try {
            Conexao.begin();

            Database.execute("DELETE FROM aluno WHERE id = " + i_id);

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public void atualizarTabela() throws Exception {
        StringBuilder sql = null;

        try {
            Conexao.begin();

            if (!Database.tabelaExiste("aluno")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE aluno (");
                sql.append(" id SERIAL, ");
                sql.append(" nome VARCHAR (50), ");
                sql.append(" matricula TEXT,");
                sql.append(" rg VARCHAR (15), ");
                sql.append(" cpf VARCHAR (15), ");
                sql.append(" CONSTRAINT pk_id_aluno PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;
        }
    }
}
