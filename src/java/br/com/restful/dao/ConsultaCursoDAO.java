package br.com.restful.dao;

import br.com.restful.model.ConsultaCursoVO;
import br.com.restful.util.Conexao;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ConsultaCursoDAO {

    public ConsultaCursoDAO() throws Exception {
        Conexao.conectar();
    }

    public ArrayList<ConsultaCursoVO> consultar() throws Exception {
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<ConsultaCursoVO> vConsultaCurso = new ArrayList();

        sql.append("SELECT c.id AS id_curso, c.descricao AS curso, d.id AS id_disciplina, d.descricao AS disciplina");
        sql.append(" FROM curso AS c");
        sql.append(" INNER JOIN cursodisciplina AS cd ON c.id = cd.id_curso");
        sql.append(" INNER JOIN disciplina AS d ON d.id = cd.id_disciplina");

        rst = Database.executeQuery(sql.toString());

        while (rst.next()) {
            ConsultaCursoVO oConsultaCurso = new ConsultaCursoVO();

            oConsultaCurso.idCurso = rst.getInt("id_curso");
            oConsultaCurso.curso = rst.getString("curso");
            oConsultaCurso.idDisciplina = rst.getInt("id_disciplina");
            oConsultaCurso.disciplina = rst.getString("disciplina");

            vConsultaCurso.add(oConsultaCurso);
        }

        Conexao.fecharConexao();

        return vConsultaCurso;
    }
}
