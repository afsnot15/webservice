package br.com.restful.dao;

import br.com.restful.model.AlunoVO;
import br.com.restful.util.Conexao;
import br.com.restful.model.DisciplinaVO;
import br.com.restful.model.EmentaVO;
import br.com.restful.tipo.DiaSemana;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DisciplinaDAO {

    public DisciplinaDAO() throws Exception {
        atualizarTabela();
    }

    public ArrayList<DisciplinaVO> consultar(String i_descricao) throws Exception {
        Conexao.conectar();
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<DisciplinaVO> vDisciplina = new ArrayList();

        sql.append("SELECT d.id, d.descricao, d.id_professor, d.cargahoraria, d.id_diasemana, d.vagas,");
        sql.append(" p.nome AS professor,");
        sql.append(" ds.descricao AS diasemana");
        sql.append(" FROM disciplina AS d");
        sql.append(" LEFT JOIN professor AS p ON p.id = d.id_professor");
        sql.append(" INNER JOIN diasemana AS ds ON ds.id = d.id_diasemana");
        sql.append(" WHERE 1 = 1");

        if (!i_descricao.isEmpty()) {
            sql.append(" AND d.descricao ILIKE '%" + i_descricao + "%'");
        }

        rst = Database.executeQuery(sql.toString());

        while (rst.next()) {
            DisciplinaVO oDisciplina = new DisciplinaVO();

            oDisciplina.id = rst.getInt("id");
            oDisciplina.descricao = rst.getString("descricao");
            oDisciplina.cargaHoraria = rst.getInt("cargahoraria");
            oDisciplina.idDiaSemana = rst.getInt("id_diasemana");
            oDisciplina.idProfessor = rst.getInt("id_professor");
            oDisciplina.vagas = rst.getInt("vagas");
            oDisciplina.professor = rst.getString("professor");
            oDisciplina.diaSemana = rst.getString("diasemana");

            vDisciplina.add(oDisciplina);
        }

        rst.close();

        Conexao.fecharConexao();

        return vDisciplina;
    }

    public DisciplinaVO carregar(int i_id) throws Exception {

        try {
            Conexao.conectar();
            ResultSet rst = null;
            ResultSet rstEmenta = null;
            ResultSet rstAluno = null;
            StringBuilder sql = new StringBuilder();
            DisciplinaVO oDisciplina = new DisciplinaVO();

            sql.append("SELECT d.id, d.descricao, d.id_professor, d.cargahoraria, d.id_diasemana, d.vagas,");
            sql.append(" p.nome AS professor,");
            sql.append(" ds.descricao AS diasemana");
            sql.append(" FROM disciplina AS d");
            sql.append(" LEFT JOIN professor AS p ON p.id = d.id_professor");
            sql.append(" INNER JOIN diasemana AS ds ON ds.id = d.id_diasemana");
            sql.append(" WHERE d.id = " + i_id);

            rst = Database.executeQuery(sql.toString());

            if (rst.next()) {
                oDisciplina.id = rst.getInt("id");
                oDisciplina.descricao = rst.getString("descricao");
                oDisciplina.idProfessor = rst.getInt("id_professor");
                oDisciplina.professor = rst.getString("professor");
                oDisciplina.vagas = rst.getInt("vagas");
                oDisciplina.idDiaSemana = rst.getInt("id_diasemana");
                oDisciplina.cargaHoraria = rst.getInt("cargahoraria");
                oDisciplina.diaSemana = rst.getString("diasemana");

                sql = new StringBuilder();
                sql.append("SELECT e.id_ementa");
                sql.append(" FROM disciplinaementa AS e");
                sql.append(" WHERE e.id_disciplina = " + oDisciplina.id);

                rstEmenta = Database.executeQuery(sql.toString());

                while (rstEmenta.next()) {
                    EmentaVO oEmenta = new EmentaDAO().carregar(rstEmenta.getInt("id_ementa"));

                    oDisciplina.v_ementa.add(oEmenta);
                }

                Conexao.conectar();

                sql = new StringBuilder();
                sql.append("SELECT a.id_aluno");
                sql.append(" FROM disciplinaaluno AS a");
                sql.append(" WHERE a.id_disciplina = " + oDisciplina.id);

                rstAluno = Database.executeQuery(sql.toString());

                while (rstAluno.next()) {
                    AlunoVO oAluno = new AlunoDAO().carregar(rstAluno.getInt("id_aluno"));

                    oDisciplina.v_aluno.add(oAluno);
                }
            }

            rst.close();
            rstEmenta.close();
            rstAluno.close();

            Conexao.fecharConexao();

            return oDisciplina;
        } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;

        }
    }

    public void salvar(DisciplinaVO i_disciplina) throws Exception {
        try {
            ResultSet rst = null;
            StringBuilder sql = new StringBuilder();

            Conexao.conectar();

            Conexao.begin();

            if (i_disciplina.id > 0) {
                sql.append("UPDATE disciplina SET");
                sql.append(" descricao = '" + i_disciplina.descricao + "'");
                sql.append(", id_diasemana = " + i_disciplina.idDiaSemana);
                sql.append(", cargahoraria = " + i_disciplina.cargaHoraria);
                sql.append(", id_professor = " + i_disciplina.idProfessor);
                sql.append(", vagas = " + i_disciplina.vagas);
                sql.append(" WHERE id = " + i_disciplina.id);

                Database.execute(sql.toString());

            } else {
                sql.append("INSERT INTO disciplina (descricao, id_diasemana, cargahoraria, id_professor, vagas) VALUES (");
                sql.append("'" + i_disciplina.descricao + "'");
                sql.append(", " + i_disciplina.idDiaSemana);
                sql.append(", " + i_disciplina.cargaHoraria);
                sql.append(", " + i_disciplina.idProfessor);
                sql.append(", " + i_disciplina.vagas);
                sql.append(")");

                Database.execute(sql.toString());

                rst = Database.executeQuery("SELECT CURRVAL('disciplina_id_seq') AS id");
                rst.next();

                i_disciplina.id = rst.getInt("id");
            }

            Database.execute("DELETE FROM disciplinaementa WHERE id_disciplina = " + i_disciplina.id);

            for (EmentaVO oEmenta : i_disciplina.v_ementa) {
                sql = new StringBuilder();
                sql.append("INSERT INTO disciplinaementa(id_disciplina, id_ementa) VALUES (");
                sql.append(i_disciplina.id + ", ");
                sql.append(oEmenta.id);
                sql.append(")");

                Database.execute(sql.toString());
            }

            Database.execute("DELETE FROM disciplinaaluno WHERE id_disciplina = " + i_disciplina.id);

            for (AlunoVO oAluno : i_disciplina.v_aluno) {
                sql = new StringBuilder();
                sql.append("INSERT INTO disciplinaaluno(id_disciplina, id_aluno) VALUES (");
                sql.append(i_disciplina.id + ", ");
                sql.append(oAluno.id);
                sql.append(")");

                Database.execute(sql.toString());
            }

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public void excluir(String i_id) throws Exception {
        try {
            Conexao.conectar();

            Conexao.begin();

            Database.execute("DELETE FROM disciplinaementa WHERE id_disciplina = " + i_id);
            Database.execute("DELETE FROM disciplinaaluno WHERE id_disciplina = " + i_id);
            Database.execute("DELETE FROM disciplina WHERE id = " + i_id);

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public boolean diaSemanaOcupado(int i_id, int i_diaSemana) throws Exception {
        Conexao.conectar();
        StringBuilder sql = new StringBuilder();
        ResultSet rst = null;

        sql.append("SELECT id");
        sql.append(" FROM  disciplina");
        sql.append(" WHERE id_diasemana = " + i_diaSemana);
        sql.append(" AND id <> " + i_id);

        rst = Database.executeQuery(sql.toString());

        return rst.next();
    }

    public boolean verificaDependencia(int i_id) throws Exception {
        Conexao.conectar();
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT id");
        sql.append(" FROM cursodisciplina");
        sql.append(" WHERE id_disciplina = " + i_id);

        rst = Database.executeQuery(sql.toString());

        return rst.next();
    }

    public void atualizarTabela() throws Exception {
        StringBuilder sql = new StringBuilder();

        Conexao.conectar();

        try {

            if (!Database.tabelaExiste("diasemana")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE diasemana (");
                sql.append(" id INTEGER, ");
                sql.append(" descricao VARCHAR(15), ");
                sql.append(" CONSTRAINT pk_id_diasemana PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());

                sql = new StringBuilder();
                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.DOMINGO.getId() + ", '");
                sql.append(DiaSemana.DOMINGO.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.SEGUNDA.getId() + ", '");
                sql.append(DiaSemana.SEGUNDA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.TERCA.getId() + ", '");
                sql.append(DiaSemana.TERCA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.QUARTA.getId() + ", '");
                sql.append(DiaSemana.QUARTA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.QUINTA.getId() + ", '");
                sql.append(DiaSemana.QUINTA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.SEXTA.getId() + ", '");
                sql.append(DiaSemana.SEXTA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO diasemana VALUES (");
                sql.append(DiaSemana.SABADO.getId() + ", '");
                sql.append(DiaSemana.SABADO.getDescricao() + "'");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("disciplina")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE disciplina (");
                sql.append(" id SERIAL, ");
                sql.append(" descricao VARCHAR (50), ");
                sql.append(" vagas INTEGER, ");
                sql.append(" id_professor INTEGER, ");
                sql.append(" cargahoraria INTEGER,");
                sql.append(" id_diasemana INTEGER,");
                sql.append(" CONSTRAINT pk_id_disciplina PRIMARY KEY (id),");
                sql.append(" CONSTRAINT fk_id_professor FOREIGN KEY (id_professor) REFERENCES professor (id),");
                sql.append(" CONSTRAINT fk_id_diasemana FOREIGN KEY (id_diasemana) REFERENCES diasemana (id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("disciplinaementa")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE disciplinaementa (");
                sql.append(" id SERIAL, ");
                sql.append(" id_ementa INTEGER, ");
                sql.append(" id_disciplina INTEGER, ");
                sql.append(" CONSTRAINT pk_id_disciplinaementa PRIMARY KEY (id),");
                sql.append(" CONSTRAINT fk_id_ementa FOREIGN KEY (id_ementa) REFERENCES ementa (id),");
                sql.append(" CONSTRAINT fk_id_disciplina FOREIGN KEY (id_disciplina) REFERENCES disciplina (id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("disciplinaaluno")) {
                sql = new StringBuilder();

                sql.append("CREATE TABLE disciplinaaluno(");
                sql.append(" id SERIAL, ");
                sql.append(" id_aluno INTEGER, ");
                sql.append(" id_disciplina INTEGER, ");
                sql.append(" CONSTRAINT pk_id_cursoaluno PRIMARY KEY (id),");
                sql.append(" CONSTRAINT fk_id_disciplina FOREIGN KEY (id_disciplina) REFERENCES disciplina(id),");
                sql.append(" CONSTRAINT fk_id_aluno FOREIGN KEY (id_aluno) REFERENCES aluno(id)");
                sql.append(" );");

                Database.execute(sql.toString());
            }

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;
        }

    }
}
