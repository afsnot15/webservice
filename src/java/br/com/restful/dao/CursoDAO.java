package br.com.restful.dao;

import br.com.restful.model.CursoVO;
import br.com.restful.util.Conexao;
import br.com.restful.model.DisciplinaVO;
import br.com.restful.model.EmentaVO;
import br.com.restful.tipo.Periodo;
import br.com.restful.tipo.TipoDuracao;
import br.com.restful.tipo.TituloProfessor;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CursoDAO {

    public CursoDAO() throws Exception {
        atualizarTabela();
    }

    public ArrayList<CursoVO> consultar(String i_descricao) throws Exception {
        Conexao.conectar();
        ResultSet rst = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<CursoVO> v_curso = new ArrayList();

        sql.append("SELECT c.id, c.descricao, c.id_tipoduracao, c.id_periodo, c.quantidadealuno, c.cargahoraria, c.duracao,");
        sql.append(" td.descricao AS tipoduracao,");
        sql.append(" p.descricao AS periodo");
        sql.append(" FROM curso AS c");
        sql.append(" INNER JOIN tipoduracao AS td ON td.id = c.id_tipoduracao");
        sql.append(" INNER JOIN periodo AS p ON p.id = c.id_periodo");
        sql.append(" WHERE 1 = 1");

        if (!i_descricao.isEmpty()) {
            sql.append(" AND c.descricao ILIKE '%" + i_descricao + "%'");
        }

        rst = Database.executeQuery(sql.toString());

        while (rst.next()) {
            CursoVO oCurso = new CursoVO();

            oCurso.id = rst.getInt("id");
            oCurso.descricao = rst.getString("descricao");
            oCurso.idTipoDuracao = rst.getInt("id_tipoduracao");
            oCurso.duracao = rst.getDouble("duracao");
            oCurso.idPeriodo = rst.getInt("id_periodo");
            oCurso.periodo = rst.getString("periodo");
            oCurso.quantidadeAluno = rst.getInt("quantidadealuno");
            oCurso.cargaHoraria = rst.getInt("cargahoraria");
            oCurso.tipoduracao = rst.getString("tipoduracao");

            sql = new StringBuilder();

            v_curso.add(oCurso);
        }

        rst.close();

        Conexao.fecharConexao();

        return v_curso;
    }

    public CursoVO carregar(int i_id) throws Exception {
        Conexao.conectar();
        ResultSet rst = null;
        ResultSet rstDisciplina = null;
        StringBuilder sql = new StringBuilder();
        CursoVO oCurso = new CursoVO();

        sql.append("SELECT c.id, c.descricao, c.id_tipoduracao, c.id_periodo, c.quantidadealuno, c.cargahoraria, c.duracao,");
        sql.append(" td.descricao AS tipoduracao,");
        sql.append(" p.descricao AS periodo");
        sql.append(" FROM curso AS c");
        sql.append(" INNER JOIN tipoduracao AS td ON td.id = c.id_tipoduracao");
        sql.append(" INNER JOIN periodo AS p ON p.id = c.id_periodo");
        sql.append(" WHERE c.id = " + i_id);

        rst = Database.executeQuery(sql.toString());

        if (rst.next()) {
            oCurso.id = rst.getInt("id");
            oCurso.descricao = rst.getString("descricao");
            oCurso.idTipoDuracao = rst.getInt("id_tipoduracao");
            oCurso.duracao = rst.getDouble("duracao");
            oCurso.idPeriodo = rst.getInt("id_periodo");
            oCurso.periodo = rst.getString("periodo");
            oCurso.quantidadeAluno = rst.getInt("quantidadealuno");
            oCurso.cargaHoraria = rst.getInt("cargahoraria");
            oCurso.tipoduracao = rst.getString("tipoduracao");

            sql = new StringBuilder();
            sql.append("SELECT d.id");
            sql.append(" FROM disciplina AS d");
            sql.append(" INNER JOIN cursodisciplina AS cd ON cd.id_disciplina = d.id");
            sql.append(" WHERE cd.id_curso = " + oCurso.id);

            rstDisciplina = Database.executeQuery(sql.toString());

            while (rstDisciplina.next()) {
                DisciplinaVO oDisciplina = new DisciplinaDAO().carregar(rstDisciplina.getInt("id"));

                oCurso.v_disciplina.add(oDisciplina);
            }
        }

        rst.close();

        Conexao.fecharConexao();

        return oCurso;
    }

    public void salvar(CursoVO i_curso) throws Exception {
        try {
            ResultSet rst = null;
            StringBuilder sql = new StringBuilder();

            Conexao.conectar();

            Conexao.begin();

            if (i_curso.id > 0) {
                sql.append("UPDATE curso SET");
                sql.append(" descricao = '" + i_curso.descricao + "'");
                sql.append(", duracao = " + i_curso.duracao);
                sql.append(", id_periodo = " + i_curso.idPeriodo);
                sql.append(", quantidadealuno = " + i_curso.quantidadeAluno);
                sql.append(", cargahoraria = " + i_curso.cargaHoraria);
                sql.append(", id_tipoduracao = " + i_curso.idTipoDuracao);
                sql.append(" WHERE id = " + i_curso.id);

                Database.execute(sql.toString());

            } else {
                sql.append("INSERT INTO curso (descricao,  id_tipoduracao, duracao, id_periodo, quantidadealuno, cargahoraria) VALUES (");
                sql.append(" '" + i_curso.descricao + "'");
                sql.append(", " + i_curso.idTipoDuracao);
                sql.append(", " + i_curso.duracao);
                sql.append(", " + i_curso.idPeriodo);
                sql.append(", " + i_curso.quantidadeAluno);
                sql.append(", " + i_curso.cargaHoraria);
                sql.append(")");

                Database.execute(sql.toString());

                rst = Database.executeQuery("SELECT CURRVAL('curso_id_seq') AS id");
                rst.next();

                i_curso.id = rst.getInt("id");
            }

            Database.execute("DELETE FROM cursodisciplina WHERE id_curso = " + i_curso.id);

            for (DisciplinaVO oDisciplina : i_curso.v_disciplina) {
                sql = new StringBuilder();
                sql.append("INSERT INTO cursodisciplina(id_curso, id_disciplina) VALUES (");
                sql.append(i_curso.id + ", ");
                sql.append(oDisciplina.id);
                sql.append(")");

                Database.execute(sql.toString());
            }

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }

    public void excluir(String i_id) throws Exception {
        try {
            Conexao.conectar();

            Conexao.begin();

            Database.execute("DELETE FROM cursodisciplina WHERE id_curso = " + i_id);
            Database.execute("DELETE FROM curso WHERE id = " + i_id);

            Conexao.commit();

            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            throw ex;
        }
    }


    public boolean professorMestreDoutor(ArrayList<DisciplinaVO> v_disciplina) throws Exception {
        Conexao.conectar();
        StringBuilder sql = new StringBuilder();
        ResultSet rst = null;

        for (DisciplinaVO oDisciplina : v_disciplina) {
            sql = new StringBuilder();

            sql.append("SELECT d.id");
            sql.append(" FROM  disciplina AS d");
            sql.append(" INNER JOIN professor AS p ON p.id = d.id_professor");
            sql.append(" INNER JOIN tituloprofessor AS tp ON tp.id = p.id_tituloprofessor");
            sql.append(" INNER JOIN cursodisciplina AS cd ON cd.id_disciplina = d.id");
            sql.append(" WHERE d.id = " + oDisciplina.id);
            sql.append(" AND (tp.id = " + TituloProfessor.DOUTOR.getId());
            sql.append(" OR tp.id = " + TituloProfessor.MESTRE.getId() + ")");

            rst = Database.executeQuery(sql.toString());

            return rst.next();
        }

        return false;
    }

    public void atualizarTabela() throws Exception {
        Conexao.conectar();
        StringBuilder sql = new StringBuilder();

        try {

            if (!Database.tabelaExiste("tipoduracao")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE tipoduracao (");
                sql.append(" id INTEGER, ");
                sql.append(" descricao VARCHAR(15), ");
                sql.append(" CONSTRAINT pk_id_tipoduracao PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());

                sql = new StringBuilder();
                sql.append("INSERT INTO tipoduracao VALUES (");
                sql.append(TipoDuracao.DIA.getId() + ", '");
                sql.append(TipoDuracao.DIA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO tipoduracao VALUES (");
                sql.append(TipoDuracao.SEMANA.getId() + ", '");
                sql.append(TipoDuracao.SEMANA.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO tipoduracao VALUES (");
                sql.append(TipoDuracao.MES.getId() + ", '");
                sql.append(TipoDuracao.MES.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO tipoduracao VALUES (");
                sql.append(TipoDuracao.ANO.getId() + ", '");
                sql.append(TipoDuracao.ANO.getDescricao() + "'");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("periodo")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE periodo (");
                sql.append(" id INTEGER, ");
                sql.append(" descricao VARCHAR(15), ");
                sql.append(" CONSTRAINT pk_id_periodo PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());

                sql = new StringBuilder();
                sql.append("INSERT INTO periodo VALUES (");
                sql.append(Periodo.MATUTINO.getId() + ", '");
                sql.append(Periodo.MATUTINO.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO periodo VALUES (");
                sql.append(Periodo.VESPERTINO.getId() + ", '");
                sql.append(Periodo.VESPERTINO.getDescricao() + "'");
                sql.append(");");

                sql.append("INSERT INTO periodo VALUES (");
                sql.append(Periodo.NOTURNO.getId() + ", '");
                sql.append(Periodo.NOTURNO.getDescricao() + "'");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("curso")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE curso (");
                sql.append(" id SERIAL, ");
                sql.append(" descricao TEXT, ");
                sql.append(" duracao NUMERIC(5,2), ");
                sql.append(" id_tipoduracao INTEGER, ");
                sql.append(" id_periodo INTEGER, ");
                sql.append(" quantidadealuno INTEGER, ");
                sql.append(" cargahoraria INTEGER, ");
                sql.append(" CONSTRAINT pk_id_curso PRIMARY KEY (id),");
                sql.append(" CONSTRAINT fk_id_tipoduracao FOREIGN KEY (id_tipoduracao) REFERENCES tipoduracao(id),");
                sql.append(" CONSTRAINT fk_id_periodo FOREIGN KEY (id_periodo) REFERENCES periodo(id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            if (!Database.tabelaExiste("cursodisciplina")) {
                sql = new StringBuilder();

                sql.append("CREATE TABLE cursodisciplina(");
                sql.append(" id SERIAL, ");
                sql.append(" id_curso INTEGER, ");
                sql.append(" id_disciplina INTEGER, ");
                sql.append(" CONSTRAINT pk_id_cursodisciplina PRIMARY KEY (id),");
                sql.append(" CONSTRAINT fk_id_curso FOREIGN KEY (id_curso) REFERENCES curso(id),");
                sql.append(" CONSTRAINT fk_id_disciplina FOREIGN KEY (id_disciplina) REFERENCES disciplina(id)");
                sql.append(" );");

                Database.execute(sql.toString());
            }

            Conexao.commit();
            Conexao.fecharConexao();

        } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;
        }
    }
}
