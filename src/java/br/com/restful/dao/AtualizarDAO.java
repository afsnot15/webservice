package br.com.restful.dao;

import br.com.restful.util.Conexao;
import br.com.restful.util.Database;
import java.sql.ResultSet;
import java.sql.Statement;

public class AtualizarDAO {

    public void atualizar() throws Exception {
        try {
            Conexao.conectar();
            Statement stm = null;
            ResultSet rst = null;
            StringBuilder sql = null;

            stm = Conexao.createStatement();

            Conexao.begin();

          

            if (!Database.tabelaExiste("aluno")) {
                sql = new StringBuilder();

                sql.append(" CREATE TABLE aluno (");
                sql.append(" id SERIAL, ");
                sql.append(" nome VARCHAR (50), ");
                sql.append(" rg VARCHAR (15), ");
                sql.append(" cpf VARCHAR (15), ");
                sql.append(" CONSTRAINT pk_id_aluno PRIMARY KEY (id)");
                sql.append(");");

                Database.execute(sql.toString());
            }

            } catch (Exception ex) {
            Conexao.rollback();
            Conexao.fecharConexao();
            throw ex;
        }
            

            

           
    }
    
    
}
